function result = moire_based_face_spoofing_detection(aY)
% Input:
%    aY: luminance channel of the input face image
% Output:
%    result = 1, if Moire patterns are found;
%             0, if Moire patterns are not found.
%
% Don't forget to cite our papers according to your application:
%    - D. C. Garcia and R. L. de Queiroz, ``Face-spoofing 2D-detection
%        based on Moiré-pattern analysis,'' IEEE Trans. on Information
%        Forensics and Security, Vol. 10, No. 4, pp. 778-786, April 2015.
%    - D. C. Garcia and R. L. de Queiroz, ``Evaluating the effects of image
%        compression in Moire-pattern-based face-spoofing detection,'' 
%        Proc. IEEE Intl. Conf. on Image Processing, ICIP, Quebec City,
%        Canada, Sep. 2015.

result = 0;
for sigma = 2.1:-.2:.1
	f = fspecial('gaussian',9,sigma)-fspecial('gaussian',9,sigma*sqrt(2));
	b = conv2(aY,f,'same');
	B = abs(fftshift(fft2(windowed_image(b))));
	n_bits = 8;
	B = round(((2^n_bits)-1)*(B-min(min(B)))/(max(max(B))-min(min(B))));
	B_entr = CorrelationThresholding(B,n_bits);
	mask_entr = B>B_entr;
	perc_entr_peaks = mean(mask_entr(:))*100;
	if perc_entr_peaks<0.1
		result = 1;
		return;
	end
end

function T = CorrelationThresholding(img, n_bits)

alphabet = 2^n_bits;
h = zeros(1,alphabet);
for levels = 0:(alphabet-1)
	h(levels+1) = sum(sum(img==levels));
end
h = h/sum(h); % Normalize the histogram so that it sums to 1.
correlation = zeros(1,alphabet); % Initialize array for storing entropies.
for t = 1:(alphabet-2)
	White = h(1:t);
	Black = h(t+1:(alphabet-1));
	correlation(t) = -log(sum(White.^2)*sum(Black.^2))+2*log(sum(White)*sum(Black));
end
[max_corr, T] = max(correlation); % The Maximal entropy determines the threshold.
T = T - 1;

function y = windowed_image(x)

[H,W] = size(x);
window_fft = hanning(H)*hanning(W)';
y = x.*window_fft;