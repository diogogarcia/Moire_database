# Moire database

This repository contains a face-spoofing database with images of 50 individuals captured under 13 different conditions:

| Condition | Display     | Capture   | Distance |
|-----------|-------------|-----------|----------|
| 0         | None        | Original  | --       |
| 1         | Macbook Pro | iPhone 4  | ≈ 20cm   |
| 2         | Macbook Pro | iPhone 4  | ≈ 30cm   |
| 3         | Macbook Pro | iPhone 4  | ≈ 40cm   |
| 4         | Macbook Pro | iPad Mini | ≈ 20cm   |
| 5         | Macbook Pro | iPad Mini | ≈ 30cm   |
| 6         | Macbook Pro | iPad Mini | ≈ 40cm   |
| 7         | iPad Mini   | iPhone 4  | ≈ 15cm   |
| 8         | iPad Mini   | iPhone 4  | ≈ 20cm   |
| 9         | iPad Mini   | iPhone 4  | ≈ 25cm   |
| 10        | iPhone 4    | iPad Mini | ≈ 10cm   |
| 11        | iPhone 4    | iPad Mini | ≈ 15cm   |
| 12        | iPhone 4    | iPad Mini | ≈ 20cm   |

If you use this database, please remember to cite the paper that motivated its creation:

> D. C. Garcia and R. L. de Queiroz, ``Face-spoofing 2D-detection based on Moiré-pattern analysis,'' _IEEE Trans. on Information Forensics and Security_, Vol. 10, No. 4, pp. 778-786, April 2015.

In this paper, face-spoofing detection was proposed by searching for Moire patterns due to the overlap of the digital grids. The conditions under which these patterns arise were described, and their detection was proposed through peak detection in the frequency domain. The proposed algorithm's performance under image compression was further assessed in:

> D. C. Garcia and R. L. de Queiroz, ``Evaluating the effects of image compression in Moire-pattern-based face-spoofing detection,'' _Proc. IEEE Intl. Conf. on Image Processing_, ICIP, Quebec City, Canada, Sep. 2015.